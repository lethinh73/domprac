// Background color droplist
document.getElementsByClassName("form-select")[0].onchange = function () {
  if (this.value != "none") {
    document.body.style.backgroundColor = this.value;
  } else {
    document.body.style.removeProperty("background-color");
  }
};

// Title color droplist
document.getElementsByClassName("form-select")[1].onchange = function () {
  if (this.value != "none") {
    for (let z = 0; z < 4; z++) {
      document.getElementsByClassName("title")[z].style.color = this.value;
    }
  } else {
    for (let z = 0; z < 4; z++) {
      document.getElementsByClassName("title")[z].style.removeProperty("color");
    }
  }
};

// Text color droplist
document.getElementsByClassName("form-select")[2].onchange = function () {
  const allP = document.getElementsByTagName("p");

  if (this.value != "none") {
    for (let i = 0; i < allP.length; i++) {
      allP[i].style.color = this.value;
    }
  } else {
    for (let i = 0; i < allP.length; i++) {
      allP[i].style.removeProperty("color");
    }
  }
};

// Link color droplist
document.getElementsByClassName("form-select")[3].onchange = function () {
  const allLinks = document.getElementsByTagName("a");

  if (this.value != "none") {
    for (let i = 0; i < allLinks.length; i++) {
      allLinks[i].style.color = this.value;
    }
  } else {
    for (let i = 0; i < allLinks.length; i++) {
      allLinks[i].style.removeProperty("color");
    }
  }
};
